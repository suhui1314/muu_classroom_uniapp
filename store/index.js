import Vue from 'vue'
import Vuex from 'vuex'
//引入微擎配置
import util from '../static/js/util.js'
Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		config: {},
		pageIndex: 0,
	},
	mutations: {
	
		setConfig(state, config) {
			state.config = config
		},
		setPageIndex(state, pageIndex) {
			state.pageIndex = pageIndex
		},
	},
    getters:{
        currentColor(state){
            return state.colorList[state.colorIndex]
        }
    },
	actions: {
		
		/**
		 * 全局配置
		 */
		setConfigAsync: async function ({commit,state}) {
			return await new Promise((resolve, reject) => {
				//TODO H5端各独立模块做个判断获取自身配置项，小程序端分包方式引入无需处理，配置项获取主包配置
				let m = util.m
				console.log(m)
				let m_config = 'muu_classroom_config'
				if(m == 'muu_classroom'){
					m_config = 'muu_classroom_config'
				}
				if(m == 'muu_minishop'){
					m_config = 'muu_minishop_config'
				}
				if(m == 'muu_scoreshop'){
					m_config = 'muu_scoreshop_config'
				}
				if(m == 'muu_activity'){
					m_config = 'muu_activity_config'
				}

				const config = uni.getStorageSync(m_config);
				if(config && config.deadtime){
					let newtime = Date.parse(new Date());
					if(config.deadtime < newtime){
						getApiData()
					}else{
						commit('setConfig', config.data)
						resolve(config)
					}
				}else{
					getApiData()
				}

				function getApiData(){
					let url = '';
						//#ifdef MP-WEIXIN
						url = 'entry/wxapp/config'
						//#endif
						//#ifdef H5
						url = 'entry/site/config'
						//#endif
					util.request({
						// 小程序端执行
						url : url,
						data: {
							action : '',
							m: m
						},
						success: function (res) {
							if (res.data.code == 200) {
								// 缓存 一个小时
								let deadtime = Date.parse(new Date()) + 3600 * 1000
								let page_data_store = {
									'deadtime' : deadtime,
									'data' : res.data.data
								}
								uni.setStorageSync(m_config, page_data_store)
								commit('setConfig', page_data_store.data)
								resolve(page_data_store)
							}
							resolve()
						}
					})
				}
			})
		},
		keepLogin: async function({commit, state}) {
			return new Promise((resolve, reject) => {
				console.log('执行keepLogin')
				var openid = uni.getStorageSync('openid') || "";
				if(openid != ''){
					util.request({
						url : 'entry/site/ucenter',
						data : {
							action: 'user_info',
							openid: openid,
							m: 'muu_classroom',
						},
						success: function (res) {
							
							if (res.data.code == 200) {
								let userInfo = {}
								//console.log(res.data.data,'用户数据')
								userInfo.memberInfo = res.data.data
								uni.setStorageSync('userInfo', userInfo)
								uni.setStorageSync('openid', res.data.data.openid)
							}else{
								uni.removeStorageSync('userInfo');
							}
							resolve(res)
						}
					})
				}
				resolve()
			})
		}
	}
})

export default store

import util from './util.js'
var jweixin = require('jweixin-module')

export default {

    //初始化sdk配置  
    initJssdk: function(callback, url) {
		url = url ? url : window.location.href;
        //console.log("init Url : "+url)
        // 这是我这边封装的 request 请求工具，实际就是 uni.request 方法。
		let all_url_arr = url.split('#')
			url = all_url_arr[0]
		util.request({
			url : 'entry/site/jssdk',
			data: {
				'url' : url
			},
			success: function (res) {
				//console.log(res.data)
				if (res.data.code == 200) {
					let result = res.data.data;
					
					jweixin.config({
					    debug: false,
					    appId: result.appId,
					    timestamp: result.timestamp,
					    nonceStr: result.nonceStr,
					    signature: result.signature,
					    jsApiList: [
					        'checkJsApi',
					        'onMenuShareTimeline',
					        'onMenuShareAppMessage',
							'scanQRCode',
							'chooseWXPay',
							'openLocation',
							'getLocation',
							'chooseImage',
							'previewImage',
							'uploadImage',
							'downloadImage'
					    ]
					});
					//配置完成后，再执行分享等功能  
					if (callback) {
					    callback(result);
					}
				}
			}
		})
    },
    //在需要自定义分享的页面中调用  
    share: function(data, url) {
        url = url ? url : window.location.href
		//console.log(url)
		let all_url_arr = url.split('#')
			url = all_url_arr[0]

		let share_link = this.shareUrl()
		//console.log(share_link)
        if (!util.isWechat()) {
            return;
        }
        //每次都需要重新初始化配置，才可以进行分享  
        this.initJssdk(function(signData) {
            jweixin.ready(function() {
				//console.log('jweixin ready')
                var shareData = {
                    title: data && data.title ? data.title : signData.site_name,
                    desc: data && data.desc ? data.desc : signData.site_description,
                    link: share_link,
                    imgUrl: data && data.img ? data.img : signData.site_logo,
                    success: function(res) {
                        // 分享后的一些操作,比如分享统计等等
                    },
                    cancel: function(res) {}
                };
                //分享给朋友接口  
                jweixin.onMenuShareAppMessage(shareData);
                //分享到朋友圈接口  
                jweixin.onMenuShareTimeline(shareData);
            });
        }, url);
    },
	
	pay: function(payObj, url){
		url = url ? url : window.location.href;
		console.log("url:"+url)
		if (!util.isWechat()) {
		    return;
		}
		//每次都需要重新初始化配置
		this.initJssdk(function(signData){
		    jweixin.ready(function() {
				console.log('jweixin ready')
				// 发起支付
		        jweixin.chooseWXPay({
					timestamp: payObj.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
					nonceStr: payObj.nonceStr, // 支付签名随机串，不长于 32 位
					package: payObj.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
					signType: 'MD5', // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
					paySign: payObj.paySign, // 支付签名
					success:function(res) {
						// 支付成功后的回调函数
						payObj.success(res)
					},
					cancel: function(res) {
						console.log(res)
					},
					fail:function(res) {
						console.log(res)
						payObj.fail(res)
					}
				});
		    });
		}, url);
	},
	
	/**
	 * 自定义分享网址
	 **/
	shareUrl:  function() {
		// 获取分销商UID(UID是否是分销商在后端判断)
		let userInfo = uni.getStorageSync('userInfo');
		let memberInfo = ''
		if(userInfo){
			memberInfo = userInfo.memberInfo
		}
		let promoter_uid = 0
		if(memberInfo && memberInfo.uid !== undefined){
			promoter_uid = memberInfo.uid
		}
		
		let url = window.location.href
		let url_arr = url.split('#');
		
		//console.log(new_url)
		// 拼接分享url地址
		let c_url = ''
		if(url_arr[1] == '/'){
			c_url = url_arr[1] + '?promoter_uid=' + promoter_uid
		}else{
			let new_url = util.urlDelP(url_arr[1], 'promoter_uid');
			//console.log(new_url)
			if(new_url.indexOf("?") != -1){
				c_url = new_url + '&promoter_uid=' + promoter_uid
			}else{
				c_url = new_url + '?promoter_uid=' + promoter_uid
			}
		}
		//console.log(c_url)
		
		// 分享的URL地址
		url = url_arr[0] + '#' + c_url
		
		console.log('h5share',url)
		
		return url;
	},
	
	/**
	 * 显示地图
	 * @param {Object} params
	 */
	location: function(params, url) {
		url = url ? url : window.location.href;
		this.initJssdk(function(signData) {
		    jweixin.ready(function() {
				console.log('jweixin ready')
				jweixin.openLocation({
					latitude: params.latitude, // 纬度，浮点数，范围为90 ~ -90
					longitude: params.longitude, // 经度，浮点数，范围为180 ~ -180。
					name: params.name, // 位置名
					address: params.address, // 地址详情说明
					scale: 16, // 地图缩放级别,整型值,范围从1~28。默认为最大
					infoUrl: '', // 在查看位置界面底部显示的超链接,可点击跳转
				});
				uni.hideLoading()
		    });
		}, url);
	}
}
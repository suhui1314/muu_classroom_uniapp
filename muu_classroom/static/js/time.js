import util from '@/static/js/util.js'

let muu = {}

muu.intervalId = {}
/**
 * 开始学习时间统计
 * @param {Object} type
 * @param {Object} type_id
 */
muu.startTimeSend = function(type,type_id){
        
	clearInterval(muu.intervalId);
	let url = ''
		//#ifdef MP-WEIXIN
		url = 'entry/wxapp/time'
		//#endif
		//#ifdef MP-MP-TOUTIAO
		url = 'entry/toutiaoapp/time'
		//#endif
		//#ifdef H5
		url = 'entry/site/time'
		//#endif
	muu.intervalId = setInterval(function(){
		//console.log(api);
		console.log('发送学习时长统计');
		
		util.request({
			url : url,
			type : "POST",
			data : {
			  'time':5,
			  'type_id' : type_id,
			  'type' : type,
			  'm' : 'muu_classroom',
			  'action' : 'add'
			},
			success:function (res) {
				console.log(res);
			}
		})
	},5000);
}

/**
 * 结束学习时间统计
 */
muu.stopTimeSend = function(){
	clearInterval(muu.intervalId);
}

module.exports = muu;
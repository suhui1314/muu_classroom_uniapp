import Vue from 'vue'
import App from './App'
import store from './store'
Vue.config.productionTip = false
Vue.prototype.$store = store

Vue.prototype.$onLaunched = new Promise(resolve => {
  Vue.prototype.$isResolve = resolve
})

import VueLazyload from 'vue-lazyload'  
Vue.use(VueLazyload)

//引入微擎配置
import util from './static/js/util.js'
Vue.prototype.util = util

// #ifdef H5
//引入微信公众号
import wechat from './static/js/wechat.js'
Vue.prototype.wechat = wechat
// #endif
// 注册全局组件
import FullscreenLoading from '@/components/muu-fullscreen-loading/muu-fullscreen-loading.vue';//全屏加载中
Vue.component('FullscreenLoading', FullscreenLoading)
import Top from "./muu_classroom/pages/common/top.vue"
Vue.component('Top', Top)
import Login from "./muu_classroom/pages/common/login.vue"
Vue.component('Login', Login)
import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue"
import MescrollUni from "@/components/mescroll-uni/mescroll-uni.vue"
Vue.component('mescroll-body', MescrollBody)
Vue.component('mescroll-uni', MescrollUni)

App.mpType = 'app'

const app = new Vue({
    ...App
})

app.$mount()

/***********************************************
function(e,t){var siteinfo=require('../siteinfo.js');var n={uniacid: siteinfo.uniacid,acid: siteinfo.acid,multiid: siteinfo.multiid,version: siteinfo.version,siteroot: siteinfo.siteroot};e.exports=n},
*/
